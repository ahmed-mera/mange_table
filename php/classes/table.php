<?php

namespace table;

spl_autoload_register(function ($class){
    require str_replace('table\\', '', $class) . '.php';
});


class table extends row implements crud {

    private $data;
    private $fileOfData = __DIR__.'/../table.json';

    public function  __construct(){

        $data = self::getDataFromJson();
        if (!empty($data))
            $this->data = json_decode($data);

    }

    public function create($data, $id){
        $table = '<table class="table table-striped table-bordered mt-5" cellspacing="0" width="100%">';
            foreach ($data as $key => $value){
                $table .= parent::create($value, $key);
            }
        $table .= "</table>";
        return $table;
    }


    public function read(){
        $data = self::getDataFromJson();
        if (empty($data)){
            return $data;
        }else{
            return self::create(json_decode(self::getDataFromJson()), null);
        }
    }


    public function delete($id){
        unset($this->data->$id );
        self::saveDataInJson($this->data);
        return json_encode(['response' => 'row is deleted successfully', 'status' => http_response_code(202)]);
        // 202 Accepted
    }

    public function deleteCeil($ids, $data){
        $idRow = $ids['idRow'];
        $idCeil = $ids['idRow'];
        $this->data->$idRow = parent::deleteCeil($idCeil, $data);
        return json_encode(['response' => 'ceil is deleted successfully', 'status' => http_response_code(202)]);
        // 202 Accepted
    }

    public function update($id, $data){
        $this->data->$id = $data;
        self::saveDataInJson($this->data);
        return json_encode(['response' => 'data is updated successfully', 'status' => http_response_code(200)]);
    }

    public function addNewRow ($data) {
        $idRow = count((array)$this->data);
        $this->data->$idRow =  $data;
        self::saveDataInJson($this->data);
        return  parent::create($data, $idRow);
    }

    public function addNewCeil($data, $id){
        $idCeil = count($this->data[$id]);
        return parent::addNewCeil($data, $idCeil);
    }

    public function setData ($data) {
        $this->data = $data;
        return json_encode(['response' => 'data set successfully', 'status' => http_response_code(200)]);
    }

    public function getData () {
        return $this->data;
    }

    public function saveDataInJson ($data){
        return file_put_contents($this->fileOfData, json_encode($data));
    }


    public function getDataFromJson (){
        return file_get_contents($this->fileOfData);
    }

    public function truncateFile () {
        // r+ Open for reading and writing; place the file pointer at the beginning of the file
        $file = fopen($this->fileOfData, 'r+');
        ftruncate($file, 0); // truncate file
        fclose($file);
        return json_encode(['response' => 'file is truncated successfully', 'status' => http_response_code(202)]);
        // 202 Accepted
    }

    public function deleteTable () {
        unset($this->data); // delete data
        return json_encode(['response' => 'table is deleted successfully', 'status' => http_response_code(202)]);
        // 202 Accepted
    }
}

