<?php

//if($_SERVER['REQUEST_METHOD'] == 'get') {
//    header('Location: ../index.html');
//}

require_once './classes/table.php';

use table\table;

$table = new table();

switch ($_POST['req']){
    case 'createTable':
        echo $table -> create($_POST['data'], null);
        break;

    case 'getTable':
        echo $table -> read();
        break;

    case 'getDataTable':
        echo $table -> getDataFromJson();
        break;

    case 'updateData':
        echo $table -> update($_POST['id'], $_POST['data']);
        break;

    case 'deleteData':
        echo $table -> delete($_POST['id']);
        break;

    case 'addNewRow':
        echo $table -> addNewRow($_POST['data']);
        break;

    default:
        echo 'no request';
        break;
}



