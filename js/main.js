/* global varibales */
let table, rows , columns, dataTable = [], PATH = './php/mange_request.php';

 let setSelect = row => {
   $(row).toggleClass('tr-color-selected').siblings().removeClass('tr-color-selected');

  rowSelected();
}


let rowSelected = () => {
  const rows = document.querySelectorAll('.tr-color-selected').length;
  $('#rowSelected').html(rows);
  rows ? $('.buttonDelete, .buttonEdit ').removeAttr('disabled') : $('.buttonDelete, .buttonEdit ').attr('disabled', true);

}




$('.createTable').click(function () {
  let inputs = $(this).parents('.container').find('.modelTable .modal-body > div > input'), // get inputs
      close = $(this).parents('.container').find('.close'), // get inputs
      nameOfRows = $('#nameOfRows').find('.modal-body'), // get inputs
      dataTaregtOld = $('.openModelTable').attr('data-target');

  if(!validate(inputs, inputs.length, 'number').includes(false)){
    rows = $(inputs[0]).val();
    columns = $(inputs[1]).val();
      createModel(nameOfRows); // headers
      close.click();
      $('.openModelTable').attr('data-target', '#nameOfRows').click();
      $('.openModelTable').attr('data-target', dataTaregtOld);
  }

})


const validate = (inputs, length, typeInput , errors = []) => {
  if (!length)
    return  errors;

  const input = $(inputs[length - 1]);
       let condition ;
   if(typeInput !== 'number')
        condition = (input.val() !== '')
   else
     condition = ( input.val() !== '' && input.val() <= parseInt(input.attr('max'))  && input.val() >= parseInt(input.attr('min')));

  if(condition){
    input.addClass('valid').removeClass('invalid');
  }else {
    input.addClass('invalid').removeClass('valid');
    errors.push(condition);
  }

  return validate(inputs.slice(0, length - 1), length - 1,typeInput , errors);
}



let sendData = data => {
  $.post(PATH, { req : 'createTable' , data }).done(res => $('.my-table').html(res));
}



$('.informationRow').click(function () {

  let inputs = $('#nameOfRows').find('.modal-body input'), // get inputs
      close = $('#nameOfRows').find('.close'), // get button close
      modalAdd = $('#modalAdd').find('.modal-body'),
      modalEdit = $('#modalEdit').find('.modal-body'),
      valuesOfRows = $('#valueOfRows').find('.modal-body'); // get inputs

  if(!validate(inputs, inputs.length, 'text').includes(false)){
    getDataHeader(inputs, inputs.length);
    createModel(valuesOfRows,dataTable[0], true);
    createModel(modalAdd,dataTable[0]); //add row
    createModel(modalEdit,dataTable[0]); //add row
    if(parseInt(rows)){
      $('.openModelTable').attr('data-target', '#valueOfRows').click();
    } else{
      showTable();
      sendData(dataTable);
    }

    close.click();
  }

})

$('.valuesRow').click(function () {

  let inputs = $('#valueOfRows').find('.modal-body input'), // get inputs
      close = $('#valueOfRows').find('.close'), // get button close
      valuesOfRows = $('#valueOfRows').find('.modal-body'), // get inputs
      dataTaregtOld = $('.openModelTable').attr('data-target');

  if(!validate(inputs, inputs.length, 'text').includes(false)){
    getDataValues(inputs);
    createModel(valuesOfRows,dataTable[0], true);
    $('.openModelTable').attr('data-target', '#valueOfRows').click();
    $('.openModelTable').attr('data-target', dataTaregtOld).click();
    showTable();
    sendData(dataTable);
    close.click();
  }

})




let createModel = (place, names = [], modals = false ) => {
  let input = '';
  if(names.length && modals){
    for (let j = 0; j < rows; j++ ){
      input += `<h4 class="modal-title w-100 font-weight-bold mb-5 mt-5"> ${j + 1})  Row </h4>`;
      for (let i = 0; i < columns; ++i){
        let name =  names[i];
        const ID = uniqid();
        input += `        
              <div class="md-form mb-4">
                    <input type="text" id="${ID}" class="form-control validate" required>
                    <label data-error="wrong" data-success="right" for="${ID}" >${name}</label>
               </div>`
      }
    }
  }else{
    for (let i = 0; i < columns; ++i){
      let name = names[i] || `Name Of Column ${i + 1}`;
      const ID = uniqid();
      input += `        
              <div class="md-form mb-4">
                    <input type="text" id="${ID}" class="form-control validate" required>
                    <label data-error="wrong" data-success="right" for="${ID}" >${name}</label>
               </div>`
    }
  }
  place.html(input);
}



const uniqid = () => Math.random().toString(16).slice(2)+(new Date()).getTime()+Math.random().toString(16).slice(2);




let getDataHeader = (inputs) => {
  let  data = [];

  for (let i = 0; i < inputs.length; ++i){
    data.push($(inputs[i]).val());
  }

  dataTable.push(data);
}


let getDataValues = inputs => {
  let j = 0;
  for (let i = 0; i < inputs.length; i++){
    let  data = [];
    if(i < Math.ceil(inputs.length / parseInt(columns))){
      let k = j;
      while (k < (parseInt(columns) + j))
        data.push($(inputs[k++]).val());

      j +=  parseInt(columns);
      dataTable.push(data);
    }
  }
}



let getTable = () => {
  $.post(PATH, {req: 'getTable'}).done( res => {
    if (res){
      $('.my-table').html(res);
       getDatatable();
       showTable();
    }else{
      $('.openModelTable').click();
    }
  })
}
getTable();


let getDatatable = () => {
  $.post(PATH, {req: 'getDataTable'}).done( resonse => {
    dataTable = JSON.parse(resonse);
    rows = dataTable.length;
    columns = dataTable[0].length;
    setModalsAdd_Edit();
  });
}


let showTable = () => {
  $('.wrapper-editor').removeClass('d-none');
  $('.openModelTable').addClass('d-none');
}


let setModalsAdd_Edit = () => {
  let modalAdd = $('#modalAdd').find('.modal-body'),
      modalEdit = $('#modalEdit').find('.modal-body');
  createModel(modalAdd,dataTable[0]); //add row
  createModel(modalEdit,dataTable[0]); //add row
}


$('.buttonEdit').click(() => setValues())


$('.editInside').click( () => edit());


function updateData(data, id){
 $.post(PATH, {req : 'updateData', id, data})
     .done(res => {
       const Toast = Swal.mixin({
         toast: true,
         position: 'top-end',
         showConfirmButton: false,
         timer: 3000,
         timerProgressBar: true,
         onOpen: (toast) => {
           toast.addEventListener('mouseenter', Swal.stopTimer)
           toast.addEventListener('mouseleave', Swal.resumeTimer)
         }
       })

       Toast.fire({
         icon: 'success',
         title: `${JSON.parse(res).response}`
       })
     })
}


function modifyRow(id) {
 let row = document.querySelector('.tr-color-selected');
 for (let i = 0; i < row.childNodes.length; i++){
   row.childNodes[i].textContent = dataTable[id][i];
 }

}

$('#btnYes').click( () => {
  const id = $('.tr-color-selected').data('id');
  $('.tr-color-selected').hide(1000, deleteRow(id));
  $('.tr-color-selected').remove();
  delete dataTable[id]
})


function deleteRow(id) {
  $.post(PATH, {req : 'deleteData', id})
      .done(res => {
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
        })

        Toast.fire({
          icon: 'success',
          title: `${JSON.parse(res).response}`
        })
      })
}


$('.buttonAdd').click(function () {
  let inputs = $('#modalAdd').find('.modal-body > div > input'),
      close = $('#modalAdd').find('.close');

      values = new Array;
  if (!validate(inputs, inputs.length).includes(false)) {
    for (let i = 0; i < inputs.length; i++) {
      values.push($(inputs[i]).val());
    }
    dataTable[Object.keys(dataTable).length] = values;
    addNewRow(values);
    close.click();
  }
})

function addNewRow(data) {
  $.post(PATH, {req : 'addNewRow', data})
      .done(res => $('.table tbody').append(res))
}

function edit() {
  let  inputs = $('#modalEdit').find('.modal-body > div > input'),
      close = $('#modalEdit').find('.close'),
      rowId = $('.tr-color-selected').data('id'),
      values = new Array;
  if (!validate(inputs, inputs.length).includes(false)){
    for (let i = 0; i < inputs.length; i++){
      values.push($(inputs[i]).val());
    }
    dataTable[rowId] = values;
    updateData(values, rowId);
    modifyRow(rowId);
    close.click();
  }

}

function setValues() {
  let rowId = $('.tr-color-selected').data('id'),
      inputs = $('#modalEdit').find('.modal-body > div > input');

  $('#modalEdit').find('.modal-body > div > label').addClass('active');
  for (let i = 0; i < dataTable[rowId].length; i++){
    $(inputs[i]).val(dataTable[rowId][i]);

  }
}

function modifyHeaders () {
  $('.table thead tr').addClass('tr-color-selected');
  $('.table tbody tr').removeClass('tr-color-selected');
   $('.buttonEdit').attr('disabled', false).click();
  $('.buttonEdit').attr('disabled', true);

  $('#modalEdit .close').on('click', function () {
    $('.table thead tr').removeClass('tr-color-selected');
    rowSelected();
  })
}


