<?php

namespace table;

interface crud {

    public function  create($data, $id);
    public function  read();
    public function  delete($id);
    public function  update($data, $id);

}
